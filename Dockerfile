FROM openjdk:11

WORKDIR /app

COPY target/nfstronks_users_api-1.jar /app/spring-app.jar

ENTRYPOINT ["java", "-jar", "spring-app.jar"]

EXPOSE 8080