package com.notfound.nfstronks_users_api.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import org.hibernate.validator.constraints.br.CPF;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Entity
@Table(name = "tb_users")
@Getter
@Setter
@Builder
@ToString
@AllArgsConstructor
@RequiredArgsConstructor
public class User {

    @Id
    @Column(name = "user_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotEmpty
    @Column(name = "user_name", nullable = false, length = 200)
    private String name;

    @NotNull
    @Column(name = "user_birthdate", nullable = false)
    private Date birthDate;

    @CPF
    @NotNull
    @Column(name = "user_cpf", nullable = false, length = 11)
    private String cpf;

    @Email
    @NotEmpty
    @Column(name = "user_email", nullable = false, length = 11)
    private String email;

    @NotNull
    @Column(name = "user_phone", nullable = false, length = 150)
    private String phone;

    @Column(name = "user_role", nullable = false, length = 5)
    private String role;

    @NotEmpty
    @Column(name = "user_password", nullable = false)
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String password;


    public void update(User newUser) {
        this.name = newUser.getName() != null ? newUser.getName() : this.name;
        this.birthDate = newUser.getBirthDate() != null ? newUser.getBirthDate() : this.birthDate;
        this.cpf = newUser.getCpf() != null ? newUser.getCpf() : this.cpf;
        this.email = newUser.getEmail() != null ? newUser.getEmail() : this.email;
        this.phone = newUser.getPhone() != null ? newUser.getPhone() : this.phone;
        this.role = newUser.getRole() != null ? newUser.getRole() : this.role;
        this.password = newUser.getPassword() != null ? newUser.getPassword(): this.password;
    }
}
