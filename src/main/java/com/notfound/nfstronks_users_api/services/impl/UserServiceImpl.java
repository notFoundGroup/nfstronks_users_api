package com.notfound.nfstronks_users_api.services.impl;

import com.notfound.nfstronks_users_api.exceptions.custom_exceptions.UserCPFAlreadyExistsException;
import com.notfound.nfstronks_users_api.exceptions.custom_exceptions.UserEmailAlreadyExistsException;
import com.notfound.nfstronks_users_api.exceptions.custom_exceptions.UserNotFoundException;
import com.notfound.nfstronks_users_api.exceptions.custom_exceptions.UserPhoneAlreadyExistsException;
import com.notfound.nfstronks_users_api.models.User;
import com.notfound.nfstronks_users_api.repositories.UserRepository;
import com.notfound.nfstronks_users_api.services.interfaces.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Primary;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
@Primary
@Qualifier("default")
public class UserServiceImpl implements UserService {
    @Autowired
    UserRepository repository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public List<User> findall() {
        return (List<User>) repository.findAll();
    }

    @Override
    public User findUserById(Long id) throws UserNotFoundException {
        Optional<User> userOptional = repository.findById(id);
        if (userOptional.isEmpty()) {
            throw new UserNotFoundException();
        }
        return userOptional.get();
    }

    @Override
    public User register(User user) throws UserCPFAlreadyExistsException, UserEmailAlreadyExistsException, UserPhoneAlreadyExistsException {
        user.setId(null);
        validateUniqueFields(user);
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        user.setRole("ROLE_USER");

        return repository.save(user);
    }

    public void validateUniqueFields(User user)
            throws UserPhoneAlreadyExistsException, UserEmailAlreadyExistsException, UserCPFAlreadyExistsException {
        Optional<User> userByPhone= repository.findByPhone(user.getPhone());
        Optional<User> userByEmail= repository.findByEmail(user.getEmail());
        Optional<User> userByCPF= repository.findByCpf(user.getCpf());

        if (userByPhone.isPresent()) {
            if (user.getId() == null || !Objects.equals(user.getId(), userByPhone.get().getId())) {
                throw new UserPhoneAlreadyExistsException();
            }
        }

        if (userByEmail.isPresent()) {
            if (user.getId() == null || !Objects.equals(user.getId(), userByEmail.get().getId())) {
                throw new UserEmailAlreadyExistsException();
            }
        }

        if (userByCPF.isPresent()) {
            if (user.getId() == null || !Objects.equals(user.getId(), userByCPF.get().getId())) {
                throw new UserCPFAlreadyExistsException();
            }
        }
    }

    @Override
    public User update(User newUser) throws UserNotFoundException, UserCPFAlreadyExistsException, UserEmailAlreadyExistsException, UserPhoneAlreadyExistsException {

        Optional<User> userOptional = repository.findById(newUser.getId());

        if (newUser.getCpf() != null || newUser.getEmail() != null || newUser.getPhone() != null) {
            validateUniqueFields(newUser);
        }

        if (userOptional.isEmpty()) {
            throw new UserNotFoundException();
        }

        if (newUser.getPassword() != null) {
            newUser.setPassword(passwordEncoder.encode(newUser.getPassword()));
        }
        User user = userOptional.get();
        user.update(newUser);

        return repository.save(user);
    }

    @Override
    public Boolean delete(Long id) throws UserNotFoundException{
        repository.deleteById(id);
        return true;
    }
}
