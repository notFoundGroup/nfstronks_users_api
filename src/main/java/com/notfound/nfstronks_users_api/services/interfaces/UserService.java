package com.notfound.nfstronks_users_api.services.interfaces;
import com.notfound.nfstronks_users_api.exceptions.custom_exceptions.UserCPFAlreadyExistsException;
import com.notfound.nfstronks_users_api.exceptions.custom_exceptions.UserEmailAlreadyExistsException;
import com.notfound.nfstronks_users_api.exceptions.custom_exceptions.UserNotFoundException;
import com.notfound.nfstronks_users_api.exceptions.custom_exceptions.UserPhoneAlreadyExistsException;
import com.notfound.nfstronks_users_api.models.User;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface UserService {

    List<User> findall();

    User register(User user) throws UserCPFAlreadyExistsException, UserEmailAlreadyExistsException, UserPhoneAlreadyExistsException;

    void validateUniqueFields(User user) throws UserPhoneAlreadyExistsException, UserEmailAlreadyExistsException, UserCPFAlreadyExistsException;

    User findUserById(Long id) throws UserNotFoundException;

    User update(User newuser) throws UserNotFoundException, UserCPFAlreadyExistsException, UserEmailAlreadyExistsException, UserPhoneAlreadyExistsException;

    Boolean delete(Long id) throws UserNotFoundException;

}

