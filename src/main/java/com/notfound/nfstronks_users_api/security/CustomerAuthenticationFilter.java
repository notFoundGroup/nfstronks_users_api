package com.notfound.nfstronks_users_api.security;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.notfound.nfstronks_users_api.dto.TokenDTO;
import com.notfound.nfstronks_users_api.exceptions.ErrorMessage;
import com.notfound.nfstronks_users_api.util.UsernamePasswordAuthenticationTokenAdapter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import static com.notfound.nfstronks_users_api.util.ProjectUtils.stackTraceToLog;
import static java.util.Arrays.stream;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@Slf4j
public class CustomerAuthenticationFilter extends OncePerRequestFilter {

    public static final String PREFIX_ATTRIBUTE = "Bearer ";

    RestTemplate restTemplate = new RestTemplate();

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
            throws ServletException, IOException {

        String authorizationHeader = request.getHeader(AUTHORIZATION);

        log.info("Verifying Headers..");
        if(authorizationHeader == null || !authorizationHeader.startsWith(PREFIX_ATTRIBUTE)){
            filterChain.doFilter(request, response);
            return;
        }

        try{
            String token = authorizationHeader.replace(PREFIX_ATTRIBUTE, "");

            TokenDTO tokenDTO = new TokenDTO(token);
            URI uri = new URI(System.getenv("API_AUTH_URL") + "/auth/authorize");
            HttpMethod method = HttpMethod.POST;

            RequestEntity<TokenDTO> requestEntity = new RequestEntity<TokenDTO>(tokenDTO, method, uri);

            log.info("Starting request to authAPI: " + requestEntity.getUrl());

            ResponseEntity<UsernamePasswordAuthenticationTokenAdapter> responseEntity =
                    restTemplate.exchange(requestEntity, UsernamePasswordAuthenticationTokenAdapter.class);

            HttpStatus status = responseEntity.getStatusCode();

            if (status.value() != 200) {
                log.error("Token Rejected.");
                filterChain.doFilter(request, response);
                return;
            }

            log.info("Getting body..");
            UsernamePasswordAuthenticationTokenAdapter usernamePasswordAuthenticationTokenAdapter = responseEntity.getBody();

            if (usernamePasswordAuthenticationTokenAdapter == null) return;

            Collection<SimpleGrantedAuthority> roles = new ArrayList<>();

            stream(usernamePasswordAuthenticationTokenAdapter.getRolesString()).
                    forEach(role -> roles.add(new SimpleGrantedAuthority(role)));

            UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken =
                    new UsernamePasswordAuthenticationToken(usernamePasswordAuthenticationTokenAdapter.getPrincipal(),
                            usernamePasswordAuthenticationTokenAdapter.getCredentials(),
                            roles);

            log.info("Successfully, doing Context for user: " + usernamePasswordAuthenticationToken.getPrincipal());

            SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);

            filterChain.doFilter(request, response);

        } catch (URISyntaxException | HttpClientErrorException | HttpServerErrorException e) {
                stackTraceToLog(e);
                filterChain.doFilter(request, response);
        } catch (Exception e){
            stackTraceToLog(e);
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            e.printStackTrace(pw);
            String stackTraceAsString = sw.toString();
            log.error(stackTraceAsString);

            response.setContentType(APPLICATION_JSON_VALUE);
            response.setStatus(500);
            ErrorMessage errorMessage = new ErrorMessage(500, new Date(), e.getMessage(), request.getServletPath());
            new ObjectMapper().writeValue(response.getOutputStream(), errorMessage);
        }
    }
}
