package com.notfound.nfstronks_users_api.security;

import com.notfound.nfstronks_users_api.exceptions.security.AccessDeniedHandlerAdapter;
import com.notfound.nfstronks_users_api.exceptions.security.EntryPointAdapter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
@Configuration
@EnableWebSecurity
public class SecurityConfig  extends WebSecurityConfigurerAdapter {
    public static final String ROLE_ADMIN = "ADMIN";

    @Autowired
    AccessDeniedHandlerAdapter accessDeniedHandlerAdapter;

    @Autowired
    EntryPointAdapter entryPointAdapter;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable().cors().and()
                .exceptionHandling().accessDeniedHandler(accessDeniedHandlerAdapter)
                .authenticationEntryPoint(entryPointAdapter)
                .and()
                .authorizeRequests()
                .antMatchers(HttpMethod.GET, "/users/current").authenticated()
                .antMatchers(HttpMethod.DELETE, "/users/*").hasRole(ROLE_ADMIN)
                .antMatchers(HttpMethod.GET, "/users").hasRole(ROLE_ADMIN)
                .antMatchers(HttpMethod.GET, "/users/*").hasRole(ROLE_ADMIN)
                .antMatchers(HttpMethod.PATCH, "/users/current").authenticated()
                .anyRequest().permitAll()
                .and()
                .addFilterBefore(new CustomerAuthenticationFilter(), UsernamePasswordAuthenticationFilter.class)
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
    }
}
