package com.notfound.nfstronks_users_api.dto;

import com.notfound.nfstronks_users_api.models.User;
import lombok.Data;
import org.hibernate.validator.constraints.br.CPF;

import javax.validation.constraints.*;
import java.util.Date;

@Data
public class UserDTO {

    public UserDTO (User user) {
        this.name = user.getName();
        this.cpf = user.getCpf();
        this.email = user.getEmail();
        this.phone = user.getPhone();
        this.birthDate = user.getBirthDate();
    }
    @NotBlank
    private String name;

    @NotBlank
    private Date birthDate;

    @CPF
    private String cpf;

    @Email
    private String email;

    @NotBlank
    @Min(11)
    @Max(11)
    private String phone;
}