package com.notfound.nfstronks_users_api.repositories;

import com.notfound.nfstronks_users_api.models.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Long> {

    Optional<User> findByPhone(String phone);
    Optional<User> findByEmail(String email);
    Optional<User> findByCpf(String cpf);

}
