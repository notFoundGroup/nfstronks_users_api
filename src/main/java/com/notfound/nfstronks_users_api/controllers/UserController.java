package com.notfound.nfstronks_users_api.controllers;

import com.notfound.nfstronks_users_api.dto.UserDTO;
import com.notfound.nfstronks_users_api.exceptions.custom_exceptions.*;
import com.notfound.nfstronks_users_api.models.User;
import com.notfound.nfstronks_users_api.services.interfaces.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static com.notfound.nfstronks_users_api.util.ProjectUtils.stackTraceToLog;


@RestController
@RequestMapping("/users")
@Controller
@CrossOrigin
@Slf4j
public class UserController {

    @Autowired
    UserService userService;

    @GetMapping
    public ResponseEntity<List<User>> findall() {
        try {
            return ResponseEntity.ok(userService.findall());
        } catch (Exception e) {
            stackTraceToLog(e);
            throw new InternalServerErrorException(e.getMessage());
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity<User> finduser(@PathVariable Long id) {
        try {
            return ResponseEntity.ok().body(userService.findUserById(id));

        } catch (UserNotFoundException e) {

            throw new BadRequestException(e.getMessage());
        } catch (Exception e) {
            stackTraceToLog(e);
            throw new InternalServerErrorException(e.getMessage());
        }
    }

    @GetMapping("/current")
    public ResponseEntity<UserDTO> getCurrent() {
        try {
            Long id = Long.parseLong(String.valueOf(SecurityContextHolder.getContext().getAuthentication().getCredentials()));

            User user = userService.findUserById(id);
            return ResponseEntity.status(200).body(new UserDTO(user));

        } catch (UserNotFoundException e) {
            throw new BadRequestException(e.getMessage());
        } catch (Exception e) {
            stackTraceToLog(e);
            throw new InternalServerErrorException(e.getMessage());
        }
    }

    @PostMapping
    public ResponseEntity<User> register(@Validated @RequestBody User user) {
        try {
            userService.register(user);
            return ResponseEntity.status(HttpStatus.CREATED).build();

        } catch (UserCPFAlreadyExistsException | UserPhoneAlreadyExistsException | UserEmailAlreadyExistsException e) {
            throw new BadRequestException(e.getMessage());
        } catch (Exception e) {
            stackTraceToLog(e);
            throw new InternalServerErrorException(e.getMessage());
        }
    }

    @PatchMapping("/current")
    public ResponseEntity<UserDTO> update(@RequestBody User newuser) {
        try {
            Long id = Long.parseLong(String.valueOf(SecurityContextHolder.getContext().getAuthentication().getCredentials()));
            newuser.setId(id);

            User user = userService.update(newuser);
            return ResponseEntity.status(204).body(new UserDTO(user));

        } catch (UserNotFoundException | UserCPFAlreadyExistsException | UserEmailAlreadyExistsException |
                UserPhoneAlreadyExistsException e) {
            throw new BadRequestException(e.getMessage());
        } catch (Exception e) {
            stackTraceToLog(e);
            throw new InternalServerErrorException(e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<User> deleteUser(@PathVariable Long id) {
        try {
            userService.delete(id);
            return ResponseEntity.ok().build();

        } catch (UserNotFoundException e) {
            throw new BadRequestException(e.getMessage());
        } catch (Exception e) {
            stackTraceToLog(e);
            throw new InternalServerErrorException(e.getMessage());
        }
    }
}
