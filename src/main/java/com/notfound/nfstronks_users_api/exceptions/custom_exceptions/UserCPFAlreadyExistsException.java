package com.notfound.nfstronks_users_api.exceptions.custom_exceptions;

public class UserCPFAlreadyExistsException extends Exception{

    public UserCPFAlreadyExistsException() {
        super("CPF já cadastrado!");

    }
}
