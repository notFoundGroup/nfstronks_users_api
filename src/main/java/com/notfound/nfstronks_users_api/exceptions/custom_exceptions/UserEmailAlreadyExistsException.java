package com.notfound.nfstronks_users_api.exceptions.custom_exceptions;

public class UserEmailAlreadyExistsException extends Exception{

    public UserEmailAlreadyExistsException() {
        super("Email já cadastrado!");

    }
}
