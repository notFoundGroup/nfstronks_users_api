package com.notfound.nfstronks_users_api.exceptions.custom_exceptions;

public class UserPhoneAlreadyExistsException extends Exception{

    public UserPhoneAlreadyExistsException() {
        super("Telefone já cadastrado!");

    }
}
