package com.notfound.nfstronks_users_api.exceptions.custom_exceptions;

public class ResourceNotFoundException extends RuntimeException{

    public ResourceNotFoundException(String message){
        super(message);
    }
}
