package testUtils;

import com.notfound.nfstronks_users_api.models.User;

import java.util.Date;

public class UserForTest {
    public static User userOK() {
        return User.builder()
                .name("Daniel Silveira")
                .cpf("30051047030")
                .birthDate(new Date())
                .email("daniel@email.com")
                .password("senha123")
                .phone("11995190019")
                .build();
    }

    public static User userWithRoleAdmin() {
        return User.builder()
                .name("Daniel Silveira")
                .cpf("30051047030")
                .birthDate(new Date())
                .email("daniel@email.com")
                .password("senha123")
                .phone("11995190019")
                .role("ROLE_ADMIN")
                .build();
    }

    public static User userFULL() {
        return User.builder()
                .id(1L)
                .name("Daniel Silveira")
                .cpf("30051047030")
                .birthDate(new Date())
                .email("daniel@email.com")
                .password("senha123")
                .phone("11995190019")
                .role("ROLE_USER")
                .build();
    }
}
