package com.notfound.nfstronks_users_api.services.impl;
import com.notfound.nfstronks_users_api.exceptions.custom_exceptions.UserCPFAlreadyExistsException;
import com.notfound.nfstronks_users_api.exceptions.custom_exceptions.UserEmailAlreadyExistsException;
import com.notfound.nfstronks_users_api.exceptions.custom_exceptions.UserNotFoundException;
import com.notfound.nfstronks_users_api.exceptions.custom_exceptions.UserPhoneAlreadyExistsException;
import com.notfound.nfstronks_users_api.models.User;
import com.notfound.nfstronks_users_api.repositories.UserRepository;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.*;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import static testUtils.UserForTest.userFULL;
import static testUtils.UserForTest.userOK;

@ExtendWith(SpringExtension.class)
class UserServiceImplTest {

    @InjectMocks
    UserServiceImpl userService;

    @Mock
    UserRepository repositoryMocked;

    @Mock
    BCryptPasswordEncoder passwordEncoder;

    @BeforeEach
    void setUp(){
       User userFULL = userFULL();
       List<User> list = new ArrayList<>();

       BDDMockito.when(repositoryMocked.findAll()).thenReturn(list);

       BDDMockito.when(repositoryMocked.save(ArgumentMatchers.any())).thenReturn(userFULL);

       BDDMockito.when(repositoryMocked.findById(ArgumentMatchers.any())).thenReturn(Optional.of(userFULL));
   }

    @Test
    @DisplayName("FindByUser returns a User When successful")
    void findByUser_returnsUser_whenSuccessful() throws UserNotFoundException {

        User userFull = userFULL();

        User userReturned = userService.findUserById(1L);

        Assertions.assertThat(userReturned).isNotNull();

        Assertions.assertThat(userReturned.getEmail()).isEqualTo(userFull.getEmail());
    }

    @Test
    @DisplayName("FindByUser UserNotFoundExc When user was not found")
    void findByUser_throwexception_whenUsernotFound() {

        BDDMockito.when(repositoryMocked.findById(ArgumentMatchers.any())).thenReturn(Optional.empty());

        Assertions.assertThatThrownBy(() -> userService.findUserById(1L)).isInstanceOf(UserNotFoundException.class);
    }

    @Test
    @DisplayName("FindAll Call repository.findall() When successful")
    void findAll_callFindAll_whenSuccessful() {

        List<User> list = userService.findall();

        Assertions.assertThat(list).isNotNull();
    }

    @Test
    @DisplayName("Register return User When successful")
    void register_returnUser_whenSuccessful() throws UserCPFAlreadyExistsException, UserEmailAlreadyExistsException, UserPhoneAlreadyExistsException {

        User userFull = userFULL();

        userFull.setPassword(passwordEncoder.encode(userFull.getPassword()));

        User userReturned = userService.register(userFull);

        Assertions.assertThat(userReturned).isNotNull();
    }

    @Test
    @DisplayName("Register Exceptions When UserCPFAlreadyExistsException")
    void register_throwExceptions_whenCPFExistsExceptions() {

        User userFull = userOK();

        BDDMockito.when(repositoryMocked.findByCpf(ArgumentMatchers.any())).thenReturn(Optional.of(userFull));

        Assertions.assertThatThrownBy(() -> userService.validateUniqueFields(userFull)).isInstanceOf(UserCPFAlreadyExistsException.class);
    }

    @Test
    @DisplayName("Register Exceptions When UserEmailAlreadyExistsException")
    void register_throwExceptions_whenEmailExistsExceptions() {

        User userFull = userOK();

        BDDMockito.when(repositoryMocked.findByEmail(ArgumentMatchers.any())).thenReturn(Optional.of(userFull));

        Assertions.assertThatThrownBy(() -> userService.validateUniqueFields(userFull)).isInstanceOf(UserEmailAlreadyExistsException.class);
    }

    @Test
    @DisplayName("Register Exceptions When UserPhoneAlreadyExistsException")
    void register_throwExceptions_whenPhoneExistsExceptions() {

        User userFull = userOK();

        BDDMockito.when(repositoryMocked.findByPhone(ArgumentMatchers.any())).thenReturn(Optional.of(userFull));

        Assertions.assertThatThrownBy(() -> userService.validateUniqueFields(userFull)).isInstanceOf(UserPhoneAlreadyExistsException.class);
    }

    @Test
    @DisplayName("Delete return True When successful")
    void delete_returnTrue_whenSuccessful() throws UserNotFoundException {

        User userFull = userFULL();

        Boolean userReturned = userService.delete(1L);

        Assertions.assertThat(userReturned).isNotNull();
    }
}